<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Result extends Model
{
    use HasFactory;

    protected $fillable = ['member_id', 'milliseconds'];

    public function member(): HasOne
    {
        return $this->hasOne(Member::class, 'id', 'member_id');
    }
}
