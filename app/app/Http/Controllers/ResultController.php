<?php

namespace App\Http\Controllers;

use App\Models\Member;
use App\Models\Result;
use App\Http\Requests\ResultRequest;
use App\Http\Resources\ResultResource;
use App\Services\ResultService;

class ResultController extends Controller
{
    public $service;

    public function __construct(ResultService $service)
    {
        $this->service = $service;
    }

    /**
     * Сохранение результата.
     *
     * @OA\Post(
     *     path="/api/results",
     *     operationId="storeResults",
     *     tags={"Результат"},
     *     summary="Сохранение результата",
     *     description="Сохранение результата для участника",
     *     @OA\Parameter(
     *         description="Email участника",
     *         in="query",
     *         name="email",
     *         required=false,
     *         example="user@site.ru"
     *     ),
     *     @OA\Parameter(
     *         description="Время",
     *         in="query",
     *         name="milliseconds",
     *         required=true,
     *         example="1400"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Данные для редактирование категории",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="data",
     *                 type="array",
     *                 @OA\Items(
     *                     @OA\Property(property="name", type="string", example="Название категории"),
     *                     @OA\Property(property="code", type="string", example="name"),
     *                     @OA\Property(property="require", type="boolean", example="true"),
     *                     @OA\Property(property="value", type="string", example="Название категории"),
     *                 )
     *             )
     *         )
     *     )
     * )
     *
     */
    public function store(ResultRequest $request)
    {
        $validData = $request->validated();

        $result = $this->service->store($validData);

        return new ResultResource($result);
    }
}
