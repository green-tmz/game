<?php

namespace App\Services;

use App\Models\Member;
use App\Models\Result;


class ResultService
{
    public function store($validData)
    {
        if (isset($validData['email'])) {
            $member = Member::firstOrCreate([
                'email' => $validData['email']
            ]);

            $result = Result::create([
                'milliseconds' => $validData['milliseconds'],
                'member_id' => $member->id
            ]);
        } else {
            $result = Result::create([
                'milliseconds' => $validData['milliseconds'],
            ]);
        };

        return $result;
    }
}
